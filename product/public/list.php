<?php

  require_once("../private/initialize.php");
  // Initialisation file.

  if (request_is_post()) {

    if ($_POST["apply"] == "delete") {
      if (isset($_POST["boxes"])) {
        $result = delete_products($_POST["boxes"]);

        if ($result != true) {
          $errors = $result;
        }
      }

    }

  }


  $page_title = "Product List";
  $stylesheet = "stylesheets/list.css";
  require("../private/shared/header.php");

  $product_set = find_all_products();

?>

<div class="block">

<form action="<?php echo "list.php"; ?>" method="post">

  <div class="header">
     <h1>Product List</h1>
     <div>

         <select name="apply" class="select">
           <option value="delete">Mass Delete Action</option>
         </select>

         <button type="submit" class="button" name="apply_button">Apply</button>

     </div>
  </div>
  <hr/>

  <div class="grid">

    <?php while ($product_arr = $product_set->fetch_assoc()) {
      $product = to_obj($product_arr); ?>
    <div class="grid_item">

      <div class="chkbox"><input type="checkbox" name="boxes[]" value="<?php echo $product_arr['id']; ?>"></div>
      <?php echo $product->sku; ?><br />
      <?php echo $product->name; ?><br />
      <?php echo $product->price . " \$"; ?><br />
      <?php echo $product->displayAttributes();?><br />

    </div>
    <?php } ?>

  </div>
</form>

</div>


<?php require("../private/shared/footer.php"); ?>
