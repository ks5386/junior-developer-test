<?php
  require_once("../private/initialize.php");
  // Initialisation file.

  if (request_is_post()) {
    switch ($_POST["product-type"]) {
      case "book" : $product = new Book($_POST); break;
      case "dvd" : $product = new DVD($_POST); break;
      case "furniture" : $product = new Furniture($_POST); break;
    }

    $errors = insert_product($product);
    if ($errors === true) {
       redirect_to("/list.php");
    }

  }

  $page_title = "Product Add";
  $stylesheet = "stylesheets/new.css";
  require("../private/shared/header.php");

?>


<div class="block">

<form action="<?php echo "new.php"; ?>" method="post">

  <div class="header">
     <h1>Product Add</h1>
     <div>
         <button type="submit" class="button" name="save">Save</button>
     </div>

     <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.0.js"></script>
     <script type="text/javascript">

        $("document").ready( function() {

          $(".select").on("change", function() {
            var val = this.value;

            $(".add-fields").load("../private/load/attr_fields.php #" + val);

          });

        });

     </script>

  </div>
  <hr />

  <div class="form">

    <div class="row">
      <div class="label">SKU</div>
      <div class="text-input"><input type="text" name="sku" class="field"/></div>
      <div class="errors"><?php if (isset($errors["sku"])) { echo $errors["sku"]; } ?></div>
    </div>

    <div class="row">
      <div class="label">Name</div>
      <div class="text-input"><input type="text" name="name" class="field"/></div>
      <div class="errors"><?php if (isset($errors["name"])) { echo $errors["name"]; } ?></div>
    </div>

    <div class="row">
      <div class="label">Price</div>
      <div class="text-input"><input type="text" name="price" class="field"/></div>
      <div class="errors"><?php if (isset($errors["price"])) { echo $errors["price"]; } ?></div>
    </div>

    <div class="row">
      <div class="label">Type Switcher</div>
      <div class="type-select">
        <select name="product-type" class="select">
          <option value="book">Book</option>
          <option value="dvd">DVD-disc</option>
          <option value="furniture">Furniture</option>
        </select>
      </div>
    </div>

    <div class="add-fields">
      <div id="book">
        <?php
          $attributes = ["weight"];
          foreach ($attributes as $attr) { ?>
            <div class="row">
              <div class="label"><?php echo ucfirst($attr); ?></div>
              <div class="text-input"><input type="text" name="<?php echo $attr; ?>" class="field"/></div>
              <div class="errors"><?php if (isset($errors["attr"])) { echo $errors["attr"]; } ?></div>
            </div>
          <?php
          } ?>
          <div class="hint">
            Please provide weight of the book in KG.
          </div>
      </div>
    </div>

  </div>
</form>

</div>

<?php require("../private/shared/footer.php"); ?>
