<?php if (!isset($page_title)) { $page_title = "Junior Developer test";} ?>

<!DOCTYPE html>

<html lang="en">
  <head>
    <title><?php echo h($page_title); ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" media="all" href="<?php echo $stylesheet; ?>"/>
  </head>

  <body>
