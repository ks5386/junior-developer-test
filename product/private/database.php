<?php

  require_once("db_credentials.php");
  /*
   * Contains credentials required for opening a connection to the MySQL server.
   *
   */

  function db_conn() {
    /*
     * Opens a new connection to the MySQL server using credentials defined in the
     * file 'db_credentials.php'. Calls function confirm_connect() that displays any errors.
     *
     * @return Object $conn that represents the connection, or bool FALSE on failure.
     *
     */
    $conn = new mysqli(servername, username, password, db_name);
    confirm_connect();
    return $conn;
  }


  function db_disconn($conn) {
    /*
     * Closes the connection to the MySQL server if any was opened.
     *
     * @param Object $conn that represents the connection, bool FALSE if the connection failed
     * or null if no connection was previously opened.
     *
     */
    if (isset($conn)) {
      $conn->close();
    }
  }


  function confirm_connect(){
    /*
     * Checks if the connection failed, in that case prints the message and terminates
     * the current script using function die().
     *
     */
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
  }


  function query_error($query_result){
    /*
     * Checks if an SQL query failed, in that case prints the message and terminates
     * the current script using function exit().
     *
     * @param bool  FALSE if the previously sent query failed, TRUE or resource on success.
     *
     */
    if (!$query_result) {
      exit("Database query failed.</br>");
    }
  }


  function create_db($conn){
    /*
     * Creates a new database with the name provided in the file 'db_cretentials.php' if one doesn't exist.
     *
     * @param Object $conn that represents the connection.
     *
     * @return String  error message in case of failure.
     *
     */
    $sql = "CREATE DATABASE IF NOT EXISTS " . $db_name;
    if ($conn->query($sql) === TRUE) {
        //echo "Database created successfully</br>";
    } else {
        return $conn->error;
    }
  }


  function create_table($conn) {
    /*
     * Creates a new table 'Products' if one doesn't exist.
     *
     * @param Object $conn that represents the connection.
     *
     * @return String  error message in case of failure.
     *
     */
    $sql = "CREATE TABLE IF NOT EXISTS Products (";
    $sql .= "id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,";
    $sql .= "sku VARCHAR(50) UNIQUE NOT NULL,";
    $sql .= "name VARCHAR(50) NOT NULL,";
    $sql .= "price FLOAT(6, 2) NOT NULL,";
    $sql .= "special VARCHAR(50) NOT NULL)";

    if ($conn->query($sql) === TRUE) {
      //echo "Table Products created successfully";
    } else {
      return $conn->error;
    }

  }

?>
