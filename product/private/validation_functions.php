<?php

  function validate_product($product) {
    /*
     * Validates data provided.
     *
     * @param Product $product  object representing product.
     *
     * @return Array $errors  that contains any validation errors as strings.
     *
     */
    $errors = array();

    /* Check the validity of product SKU: whether it is not blank,
       if it is unique and of a valid length. */
    if (is_blank($product->sku)) {
      $errors["sku"] = "SKU cannot be blank.";
    } elseif (!valid_length($product->sku, 5, 15)) {
      $errors["sku"] = "SKU length must be between 5 and 15 characters.";
    } elseif (!unique_sku($product->sku)) {
      $errors["sku"] = "Given SKU is already in use.";
    }

    /* Check the validity of product Name: whether it is not blank
       and of a valid length. */
    if (is_blank($product->name)) {
      $errors["name"] = "Name cannot be blank.";
    } elseif (!valid_length($product->name, 2, 25)) {
      $errors["name"] = "Name length must be between 2 and 25 characters.";
    }

    /* Check the validity of product Price: whether it is not blank,
      if it is a number and of a valid length. */
    if (is_blank($product->price)) {
      $errors["price"] = "Price cannot be blank.";
    } elseif (!is_numeric($product->price)) {
      $errors["price"] = "Please provide a number";
    } elseif (!valid_length($product->price, 1, 8)) {
      $errors["price"] = "Price cannot be blank or greater than 999999";
    }

    /* Check the validity of product Special attributes: whether fiels are not blank,
       and if they are numbers. */
    if (empty($product->getAttributes())) {
      $errors["attr"] = "This field cannot be blank";
    } elseif (!all_numeric($product->getAttributes())) {
      $errors["attr"] = "Please provide a number.";
    }

    return $errors;
  }


  function is_blank($string) {
   /*
    * Checks if the string is empty.
    *
    * @param String $string  text to be checked.
    *
    * @return bool  TRUE if the string is empty, FALSE otherwise.
    *
    */
    return !isset($string) || trim($string) === "";
  }


  function valid_length($string, $min, $max) {
   /*
    * Checks if length of a given string is greater or equal to $min and
    * less or equal to $max.
    *
    * @param String $string  text to be checked
    * @param integer $min  minimal allowed length
    * @param integer $max  maximal allowed length
    *
    * @return bool  TRUE if the length is valid, FALSE otherwise.
    *
    */
    return !( (strlen($string) < $min) || (strlen($string) > $max) );
  }


  function unique_sku($string) {
   /*
    * Checks if SKU of the product is not a duplicate.
    *
    * @param String $string  SKU to be checked.
    *
    * @return bool  TRUE if there is no records with the given SKU in the database,
    * FALSE otherwise.
    *
    */
    global $db;
    $sql = "SELECT id FROM Products WHERE sku='" . $string . "'";
    $result = $db->query($sql);
    return ($result->num_rows == 0);
  }


  function all_numeric($array) {
    /*
     * Checks if all elements in the array are numbers.
     *
     * @param Array $array  to be checked.
     *
     * @return bool  TRUE if all elements are numeric, FALSE otherwise.
     *
     */
    foreach ($array as $attr) {
      if (!is_numeric($attr)) {
        return false;
      }
    }
    return true;
  }


?>
