<?php

  function gen_url($path) {
    /*
     * Generates correct URL for the given page.
     *
     * @param String $path  path to the page file.
     *
     * @return String  path from the website root (public directory) to the page file.
     *
     */
		if ($path[0] != '/') {
			$path = '/' . $path;
		}
		return 	WWW_ROOT . $path;
	}


  function redirect_to($location) {
    /*
     * Redirects to another page.
     *
     * @param String $location  page to be redirected to.
     *
     */
    header("Location: " . gen_url($location));
    exit();
  }


  function request_is_post() {
    /*
     * Checks if the request is POST.
     *
     * @return bool  TRUE if POST, FALSE otherwise.
     *
     */
		return ($_SERVER['REQUEST_METHOD'] == 'POST');
	}


  function h($string='') {
    /*
     * Converts special characters to HTML entities.
     *
     * @param String $string  text to be converted.
     *
     * @return String  converted text.
     *
     */
		return htmlspecialchars($string);
	}


	function u($string='') {
    /*
     * URL-encodes string.
     *
     * @param String $string  text to be encoded.
     *
     * @return String  encoded text.
     *
     */
		return urlencode($string);
	}


  function to_obj($product_arr) {
    /*
     * Converts an associative array to an instance of class Product.
     *
     * @param Array $product_arr  array that contains required parameters.
     *
     * @return Product $product  instance of class Product.
     *
     */
    if (strpos($product_arr["special"], "KG") !== false) {
      $product = new Book($product_arr);
    } elseif (strpos($product_arr["special"], "MB") !== false) {
      $product = new DVD($product_arr);
    } else {
      $product = new Furniture($product_arr);
    }
    $product->attrFromString($product_arr["special"]);
    return $product;
  }

?>
