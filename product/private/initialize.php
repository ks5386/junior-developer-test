<?php
/*
 * Initialisation file.
 * Opens a connection to the MySQL server, cretes Database Products and Table Products
 * if they hadn't been previously created.
 *
 */

  ob_start();

  $public_end = strpos($_SERVER['SCRIPT_NAME'], '/public') + 7;
  $doc_root = substr($_SERVER['SCRIPT_NAME'], 0, $public_end);
  define("WWW_ROOT", $doc_root); // define website root: path to public directory

  require_once('functions.php');
 // Contains general php functions.
  require_once('classes/product_classes.php');
 // Contains classes Product and its child-classes Book, DVD and Furniture.
  require_once('database.php');
 // Contains functions involving database.
  require_once('validation_functions.php');
 // Contains data validation functions.
  require_once('query_functions.php');
 // Contains functions performing MySQL queries.

  $db = db_conn();              // connect to MySQL
  $db_err = create_db($db);     // create database if it doesn't exist
  $tb_err = create_table($db);  // create table if it doesn't exist

?>
