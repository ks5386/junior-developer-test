<?php

  function insert_product($product) {
   /*
    * Inserts data in the table Products. Calls function validate_product() that returns an array
    * containing any validation errors. If the array is empty (no errors) sends an SQL query,
    * Otherwise returns the array.
    * In case of query failure closes the connection to the MySQL server terminates
    * the current script using function exit().
    *
    * @param Product $product  object representing product.
    *
    * @return Array $errors  that contains validation errors, or bool TRUE on query success.
    *
    */
    global $db;
    $errors = validate_product($product);
    if (!empty($errors)) {
       return $errors;
    }

    $product->price = sprintf("%01.2f", $product->price);

    $sql = "INSERT INTO Products (sku, name, price, special) ";
    $sql .= "VALUES (";
    $sql .= "'" . h($db->real_escape_string($product->sku)) . "', ";
    $sql .= "'" . h($db->real_escape_string($product->name)) . "', ";
    $sql .= "'" . $db->real_escape_string($product->price) . "', ";
    $sql .= "'" . $db->real_escape_string($product->displayAttributes()) . "'";
    $sql .= ")";

    if ($db->query($sql) === TRUE){
      return true;
    } else {
      echo $db->error;
      db_disconn($db);
      exit();
    }

  }


  function find_all_products() {
   /*
    * Selects all data from the table Products.
    *
    * @return MySQL $result  resource to the query result, or bool FALSE on error.
    *
    */
    global $db;
    $sql = "SELECT * from Products ";
    $sql .= "ORDER BY name ASC";
    $result = $db->query($sql);
    return $result;
  }


  function delete_products($id_set) {
   /*
    * Deletes records specified by id from the table Products. In case of query failure
    * prints the error message, disconnects from the MySQL server and terminates the
    * current script using function exit();
    *
    * @param Array of String $id_set  that contains id of all checked checkboxes.
    *
    * @return bool  TRUE on success.
    *
    */
    global $db;
    foreach ($id_set as $id) {

      $sql = "DELETE FROM Products ";
      $sql .= "WHERE id='" . $db->real_escape_string($id) . "'";

      if ($db->query($sql) !== TRUE){
        echo $db->error;
        db_disconn($db);
        exit();
      }

    }
    return true;
  }

?>
