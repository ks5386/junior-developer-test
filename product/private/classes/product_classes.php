<?php

  class Product {
   /*
    * Represents a product that has its unique SKU - String $sku,
    * Name - String $name and Price - float $price.
    *
    */
    public $sku;
    public $name;
    public $price;

    function __construct($post) {
     /*
      * Constructor.
      *
      * @param Array $post that contains the elements of $_POST array.
      *
      */
      $this->sku = isset($post["sku"]) ? $post["sku"] : "";
      $this->name = isset($post["name"]) ? $post["name"] : "";
      $this->price = isset($post["price"]) ? $post["price"] : "";
    }
  }


  class Book extends Product {
   /*
    * Child of Product class. Represents a book that has an additional attribute Weight.
    *
    */
    public $weight;


    function __construct($post) {
     /*
      * Constructor. Additionally sets Weight of a book.
      *
      * @param Array $post that contains elements of $_POST array.
      *
      */
      parent::__construct($post);
      $this->weight = isset($post["weight"]) ? $post["weight"] : "";
    }

    function displayAttributes() {
     /*
      * Creates a string that represents Weight in the format suitable for
      * displaying on a webpage and storing in the database.
      *
      * @return String text representing Weight.
      *
      */
      return "Weight: " . $this->weight . " KG";
    }

    function getAttributes() {
      /*
       * Creates an array containing Weight of this Book.
       *
       * @return Array of integer.
       *
       */
      if ($this->weight != "") {
        return array($this->weight);
      } else {
        return array();
      }
    }

    function attrFromString($string) {
     /*
      * Extracts from a string and sets Weight of this Book.
      *
      * @param String $string that represent Weight.
      *
      */
      $this->weight = substr($string, 8, -3);
    }

  }


  class DVD extends Product {
   /*
    * Child of Product class. Represents a DVD-disc that has an additional attribute Size.
    *
    */
    public $size;

    function __construct($post) {
      /*
      * Constructor. Additionally sets Size of a DVD-disc.
      *
      * @param Array $post that contains elements of $_POST array.
      *
      */
      parent::__construct($post);
      $this->size = isset($post["size"]) ? $post["size"] : "";
    }

    function displayAttributes() {
     /*
      * Creates a string that represents Size in the format suitable for
      * displaying on a webpage and storing in the database.
      *
      * @return String text representing Size.
      *
      */
      return "Size: " . $this->size . " MB";
    }

    function getAttributes() {
      /*
      * Creates an array containing Size of this DVD-disc.
      *
      * @return Array of integer.
      *
      */
      if ($this->size != "") {
        return array($this->size);
      } else {
        return array();
      }
    }

    function attrFromString($string) {
     /*
      * Extracts from a string and sets Size of this DVD-disc.
      *
      * @param String $string that represent Size.
      *
      */
      $this->size = substr($string, 6, -3);
    }

  }


  class Furniture extends Product {
   /*
    * Child of Product class. Represents furniture that has additional attributes Height,
    * Width and Length.
    *
    */
    public $height;
    public $width;
    public $length;

    function __construct($post) {
     /*
      * Constructor. Additionally sets Height, Width and Length.
      *
      * @param Array $post that contains the elements of $_POST array.
      *
      */
      parent::__construct($post);
      $this->height = isset($post["height"]) ? $post["height"] : "";
      $this->width = isset($post["width"]) ? $post["width"] : "";
      $this->length = isset($post["length"]) ? $post["length"] : "";
    }

    function displayAttributes(){
      /*
      * Creates a string that represents Height, Width and Length in the format suitable for
      * displaying on a webpage and storing in the database.
      *
      * @return String text representing Height, Width and Length.
      *
      */
      return "Dimensions: " . $this->height . "x" . $this->width . "x" . $this->length;
    }

    function getAttributes() {
      /*
      * Creates an array containing Height, Width and Length of this Furniture.
      *
      * @return Array of integer.
      *
      */
      if ($this->height != "" && $this->width != "" && $this->length != "") {
        return array($this->height, $this->width, $this->length);
      } else {
        return array();
      }
    }

    function attrFromString($string) {
     /*
      * Extracts from a string and sets Height, Width and Length of this Furniture.
      *
      * @param String $string that represent Height, Width and Length.
      *
      */
      $string = substr($string, 12);
      $x_pos = strpos($string, "x");
      $this->height = substr($string, 0, $x_pos);
      $string = substr($string, $x_pos + 1);

      $x_pos = strpos($string, "x");
      $this->width = substr($string, 0, $x_pos);
      $string = substr($string, $x_pos + 1);

      $this->length = $string;
    }

  }

?>
