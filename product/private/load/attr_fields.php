<div id="book">
  <?php
    $attributes = ["weight"];
    foreach ($attributes as $attr) { ?>
      <div class="row">
        <div class="label"><?php echo ucfirst($attr); ?></div>
        <div class="text-input"><input type="text" name="<?php echo $attr; ?>" class="field"/></div>
        <div class="errors"><?php if (isset($errors["attr"])) { echo $errors["attr"]; } ?></div>
      </div>
    <?php
    } ?>
    <div class="hint">
      Please provide weight of the book in KG.
    </div>
</div>

<div id="dvd">
  <?php
    $attributes = ["size"];
    foreach ($attributes as $attr) { ?>
      <div class="row">
        <div class="label"><?php echo ucfirst($attr); ?></div>
        <div class="text-input"><input type="text" name="<?php echo $attr; ?>" class="field"/></div>
        <div class="errors"><?php if (isset($errors["attr"])) { echo $errors["attr"]; } ?></div>
      </div>
    <?php
    } ?>
    <div class="hint">
      Please provide size of the DVD in MB.
    </div>
</div>

<div id="furniture">
  <?php
    $attributes = ["height", "width", "length"];
    foreach ($attributes as $attr) { ?>
      <div class="row">
        <div class="label"><?php echo ucfirst($attr); ?></div>
        <div class="text-input"><input type="text" name="<?php echo $attr; ?>" class="field"/></div>
        <div class="errors"><?php if (isset($errors["attr"])) { echo $errors["attr"]; } ?></div>
      </div>
    <?php
    } ?>
    <div class="hint">
      Please provide dimensions of the furniture in centimeters (cm).
    </div>
</div>
